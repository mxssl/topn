# topn

topn finds top N numbers in a file

## Installation

### Option 1 - Compilation

1) Install golang

[Install](https://golang.org/doc/install)

2) Install dep package manager

```
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
```

3) Execute these commands for building a binary

```
mkdir -p ${GOPATH}/src/gitlab.com/mxssl/topn && cd ${GOPATH}/src/gitlab.com/mxssl/topn
git clone git@gitlab.com:mxssl/topn.git .
make dep
make build
chmod +x topn
```

### Option 2 - Download compiled binary

[MacOS](https://github.com/mxssl/temp/releases/download/0.0.1/topn-darwin-amd64)

[Linux](https://github.com/mxssl/temp/releases/download/0.0.1/topn-linux-amd64)

[Windows](https://github.com/mxssl/temp/releases/download/0.0.1/topn-windows-amd64.exe)

#### Example

```
wget https://github.com/mxssl/temp/releases/download/0.0.1/topn-linux-amd64 -O topn
chmod +x topn
./topn --help
```

## Usage

### Generate file

`topn generate --lines N --name filename` - generate `file` with `N` lines of random numbers.

### Find the top N of largest numbers

`topn run --file filename --top N`

### Example

1. Generate file `numbers.txt` with `1000` lines of random numbers

```
./topn generate --name numbers.txt --lines 1000
Successfully generate file numbers.txt with 1000 lines of random numbers
```

2. Find top 5 largest numbers in file `numbers.txt`

```
./topn run --file numbers.txt --top 5
[9219151525891662339 9215619702456294450 9207997407084704522 9207450753580197605 9173624551887931713]
```

## Time and space complexity

```
Time: O(n)
Space: O(n)
```

## How to improve

One of the way to improve this program is [External merge sort](https://en.wikipedia.org/wiki/External_sorting#External_merge_sort)
